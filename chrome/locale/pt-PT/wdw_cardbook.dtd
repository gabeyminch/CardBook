<!ENTITY wdw_cardbookWindowLabel "CardBook">

<!ENTITY enableAccountsTooltip "Ativar ou desativar a procura e a sincronização de livros de endereços.">
<!ENTITY colorAccountsTooltip "Colorir os resultados da pesquisa de livros de endereços (usar quando as pesquisas abrangem vários livros de endereços).">
<!ENTITY readonlyAccountsTooltip "Colocar livros de endereços em modo só de leitura ou em modo de leitura e escrita.">
<!ENTITY searchRemoteLabel "Este livro de endereços mostra os contactos apenas após uma pesquisa">

<!ENTITY cardbookAccountMenuLabel "Livro de Endereços">

<!ENTITY cardbookAccountMenuAddServerLabel "Novo Livro de Endereços">
<!ENTITY cardbookAccountMenuEditServerLabel "Editar Livro de Endereços">
<!ENTITY cardbookAccountMenuCloseServerLabel "Apagar Livro de Endereços">
<!ENTITY cardbookAccountMenuSyncLabel "Sincronizar Livro de Endereços">
<!ENTITY cardbookAccountMenuSyncsLabel "Sincronizar Todos os Livros de Endereços">

<!ENTITY cardbookContactsMenuLabel "Contactos">

<!ENTITY cardbookToolsMenuLabel "Ferramentas">
<!ENTITY cardbookToolsMenuBirthdayListLabel "Ver Lista de Aniversários">
<!ENTITY cardbookToolsMenuSyncLightningLabel "Adicionar Aniversários ao Calendário">
<!ENTITY cardbookToolsMenuFindAllDuplicatesLabel "Encontrar Contactos Duplicados em todos os Livros de Endereços">
<!ENTITY cardbookToolsMenuFindSingleDuplicatesLabel "Encontrar Contactos Duplicados no Livro de Endereços Atual">
<!ENTITY cardbookToolsMenuPrefsLabel "Preferências do CardBook">

<!ENTITY cardbookToolbarLabel "Barra de ferramentas do CardBook">
<!ENTITY cardbookToolbarAccesskey "C">
<!ENTITY cardbookABPaneToolbarLabel "Barra de Ferramentas do Painel do Livro de Endereços do CardBook">
<!ENTITY cardbookABPaneToolbarAccesskey "d">
<!ENTITY CustomizeCardBookToolbarLabel "Personalizar…">
<!ENTITY CustomizeCardBookToolbarAccesskey "P">
<!ENTITY cardbookToolbarAddServerButtonLabel "Novo Livro de Endereços">
<!ENTITY cardbookToolbarAddServerButtonTooltip "Adicionar um livro de endereços local ou remoto">
<!ENTITY cardbookToolbarSyncButtonLabel "Sincronizar">
<!ENTITY cardbookToolbarSyncButtonTooltip "Sincronizar todos os livros de endereços remotos">
<!ENTITY cardbookToolbarWriteButtonLabel "Escrever">
<!ENTITY cardbookToolbarWriteButtonTooltip "Criar mensagem">
<!ENTITY cardbookToolbarChatButtonLabel "Ligar">
<!ENTITY cardbookToolbarChatButtonTooltip "Enviar mensagem instantânea ou de chat">
<!ENTITY cardbookToolbarConfigurationButtonLabel "Preferências">
<!ENTITY cardbookToolbarConfigurationButtonTooltip "Preferências do CardBook">
<!ENTITY cardbookToolbarAddContactButtonLabel "Novo Contacto">
<!ENTITY cardbookToolbarAddContactButtonTooltip "Criar contacto no livro de endereços">
<!ENTITY cardbookToolbarAddListButtonLabel "Nova lista">
<!ENTITY cardbookToolbarAddListButtonTooltip "Nova Lista de Correspondência">
<!ENTITY cardbookToolbarEditButtonLabel "Editar">
<!ENTITY cardbookToolbarEditButtonTooltip "Editar o contacto selecionado">
<!ENTITY cardbookToolbarRemoveButtonLabel "Apagar">
<!ENTITY cardbookToolbarRemoveButtonTooltip "Apagar contacto selecionado">
<!ENTITY cardbookToolbarPrintButtonLabel "Visualização de impressão">
<!ENTITY cardbookToolbarPrintButtonTooltip "Permitir a impressão do seleccionado">
<!ENTITY cardbookToolbarBackButtonLabel "Desfazer">
<!ENTITY cardbookToolbarBackButtonTooltip "Retroceder uma ação">
<!ENTITY cardbookToolbarForwardButtonLabel "Refazer">
<!ENTITY cardbookToolbarForwardButtonTooltip "Avançar uma ação">
<!ENTITY cardbookToolbarAppMenuButtonLabel "Menu CardBook">
<!ENTITY cardbookToolbarAppMenuButtonTooltip "Menu CardBook">
<!ENTITY cardbookToolbarSearchBoxLabel "Procura de contactos">
<!ENTITY cardbookToolbarSearchBoxTooltip "Procura de contactos">
<!ENTITY cardbookToolbarComplexSearchLabel "Procura Guardada">
<!ENTITY cardbookToolbarComplexSearchTooltip "Criar uma pasta de pesquisa guardada">
<!ENTITY cardbookToolbarThMenuButtonLabel "AppMenu">
<!ENTITY cardbookToolbarThMenuButtonTooltip "Mostrar o &brandShortName; Menu">
<!ENTITY cardbookToolbarABMenuLabel "Vista de Livros de Endereços">
<!ENTITY cardbookToolbarABMenuTooltip "Filtrar como os livros de endereços são mostrados">

<!ENTITY generalTabLabel "Geral">
<!ENTITY mailPopularityTabLabel "E-mail popularidade">
<!ENTITY technicalTabLabel "Técnico">
<!ENTITY vCardTabLabel "vCard">

<!ENTITY categoriesGroupboxLabel "Categorias">
<!ENTITY noteTabLabel "Notas">

<!ENTITY miscGroupboxLabel "Diversos">
<!ENTITY othersGroupboxLabel "Outros">
<!ENTITY techGroupboxLabel "Técnico">
<!ENTITY labelGroupboxLabel "Etiquetas">
<!ENTITY class1Label "Classes">
<!ENTITY geoLabel "Geo">
<!ENTITY mailerLabel "Mailer">
<!ENTITY agentLabel "Agente">
<!ENTITY keyLabel "Chave">
<!ENTITY photolocalURILabel "Imagem local">
<!ENTITY logolocalURILabel "Logótipo local">
<!ENTITY soundlocalURILabel "Som local">
<!ENTITY photoURILabel "Fotografia">
<!ENTITY logoURILabel "Logótipo">
<!ENTITY soundURILabel "Som">
<!ENTITY prodidLabel "Prodid">
<!ENTITY sortstringLabel "Ordenar String">
<!ENTITY uidLabel "Contacto id">
<!ENTITY versionLabel "Versão">
<!ENTITY tzLabel "Fuso horário">

<!ENTITY dirPrefIdLabel "Livro de Endereços id">
<!ENTITY cardurlLabel "URL do Cartão">
<!ENTITY cacheuriLabel "Cache URI">
<!ENTITY revLabel "Última Atualização">
<!ENTITY etagLabel "Etag">

<!ENTITY localizeadrTreeLabel "Localizar no Mapa">
<!ENTITY toEmailEmailTreeLabel "Escrever Nova Mensagem">
<!ENTITY ccEmailEmailTreeLabel "Escrever Nova Mensagem (cópia / Cc)">
<!ENTITY bccemailemailTreeLabel "Escrever Nova Mensagem (bcc)">
<!ENTITY findemailemailTreeLabel "Encontrar E-mails Relacionados com este Endereço de E-mail">
<!ENTITY findeventemailTreeLabel "Encontrar Eventos no Calendário relacionados com este Endereço de E-mail">
<!ENTITY openURLTreeLabel "Abrir URLs">

<!ENTITY toEmailCardFromAccountsOrCatsLabel "Escrever Nova Mensagem">
<!ENTITY toEmailCardFromCardsLabel "Escrever Nova Mensagem">
<!ENTITY ccEmailCardFromAccountsOrCatsLabel "Escrever Nova Mensagem (cópia / Cc)">
<!ENTITY ccEmailCardFromCardsLabel "Escrever Nova Mensagem (cópia / Cc)">
<!ENTITY bccEmailCardFromAccountsOrCatsLabel "Escrever Nova Mensagem (Bcc)">
<!ENTITY bccEmailCardFromCardsLabel "Escrever Nova Mensagem (Bcc)">
<!ENTITY shareCardByEmailFromAccountsOrCatsLabel "Partilhar por E-mail">
<!ENTITY shareCardByEmailFromCardsLabel "Partilhar por E-mail">
<!ENTITY categoryLabel "Categoria">
<!ENTITY findEmailsFromCardsLabel "Encontrar E-mails Relacionados com este Endereço de E-mail">
<!ENTITY findEventsFromCardsLabel "Encontrar Eventos no Calendário Relacionados com este Contacto">
<!ENTITY localizeCardFromCardsLabel "Localizar no Mapa">
<!ENTITY openURLCardFromCardsLabel "Abrir URLs">
<!ENTITY cutCardFromAccountsOrCatsLabel "Cortar">
<!ENTITY cutCardFromCardsLabel "Cortar">
<!ENTITY copyCardFromAccountsOrCatsLabel "Copiar">
<!ENTITY copyCardFromCardsLabel "Copiar">
<!ENTITY pasteCardFromAccountsOrCatsLabel "Colar">
<!ENTITY pasteCardFromCardsLabel "Colar">
<!ENTITY pasteEntryLabel "Colar Entrada">
<!ENTITY exportCardToFileLabel "Exportar para Ficheiro">
<!ENTITY exportCardToDirLabel "Exportar para Diretoria">
<!ENTITY importCardFromFileLabel "Importar Contactos de um Ficheiro">
<!ENTITY importCardFromDirLabel "Importar Contactos de uma Diretoria">
<!ENTITY findDuplicatesFromAccountsOrCatsLabel "Encontrar Contactos Duplicados no Livro de Endereços Atual">
<!ENTITY generateFnFromAccountsOrCatsLabel "Gerar Nomes a Mostrar">
<!ENTITY mergeCardsFromCardsLabel "Fundir Contactos">
<!ENTITY duplicateCardFromCardsLabel "Duplicar Contacto">
<!ENTITY convertListToCategoryFromCardsLabel "Converter Lista para Categoria">
<!ENTITY editAccountFromAccountsOrCatsLabel "Editar Livro de Endereços">
<!ENTITY syncAccountFromAccountsOrCatsLabel "Sincronizar Livro de Endereços">
<!ENTITY removeAccountFromAccountsOrCatsLabel "Apagar Livro de Endereços">

<!ENTITY IMPPMenuLabel "Ligar A">
